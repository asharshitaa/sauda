Voice-Automated Mazee Game 

 A web-based maze game where players navigate using both voice
 commands and keyboard arrow keys. Built with HTML, CSS,
 JavaScript, and Phaser.js, it integrates the Web Speech API for
 voice control, providing an innovative gameplay experience.



# VAMG
