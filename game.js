console.log('started')

const config = {
  type: Phaser.AUTO,
  width: 830,
  height: 620,
  backgroundColor: '#fff',
  parent: 'game-container',
  scene: {
      preload: preload,
      create: create,
      update: update
  },
  physics: {
      default: 'arcade',
      arcade: {
          gravity: { y: 0 },
          debug: false
      }
  },
  scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH
  }
};

const game = new Phaser.Game(config);
let player;
let cursors;
const mazeSize = 30;
let currentLevelIndex = 0;
let walls;
let command = '';
let popupShown = false;
let next_level = false;

const levels = {
  easy: [ 
    {
      maze: [ 
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1],
        [1,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1],
        [1,1,1,1,1,1,1,0,1,0,1,0,1,0,0,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,1,0,1],
        [1,0,1,1,1,0,1,1,1,0,1,0,1,0,0,1,0,1],
        [1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,1,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,14,1],
      ],
      entry: { x: 1, y: 3 },
      exit: { x: 21, y: 20}
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,0,1],
        [1,0,0,0,1,0,1,0,1,0,1,0,0,1,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,0,0,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,1],
        [1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,1,0,1],
        [1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,14,1,1,1,1],
      ],
      entry: { x: 1, y: 3 },
      exit: { x: 18, y: 19}
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,1],
        [1,0,1,0,0,0,1,0,1,0,1,0,1,1,1,1,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,1,0,1,0,0,0,0,1,1,1,1,1],
        [1,1,1,1,1,0,1,0,1,1,1,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,1,0,0,0,1,1,1,1,1,1,0,1],
        [1,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,0,0,0,0,1,1,1,1,1,1,1,1,0,1],
        [1,0,1,0,0,1,1,0,1,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,1,0,1,0,1,1,1,1,1,1,1,1],
        [1,1,1,1,0,0,1,0,1,0,1,0,0,0,1,0,0,1],
        [1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,0,1],
        [1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,1],
        [1,14,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
      ],
      entry: { x: 1, y: 3 },
      exit: { x: 6 , y: 20}
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1],
        [1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,1,1,1],
        [1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,0,1],
        [1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1],
        [1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,0,1,0,1,1,1,1,1],
        [1,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1],
        [1,14,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
      ],
      entry: { x: 1, y: 3 },
      exit: { x: 6 , y: 20}
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,0,1],
        [1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,1],
        [1,1,1,1,1,0,1,0,1,1,1,1,1,1,0,1,0,1],
        [1,0,0,0,0,0,1,0,1,0,0,0,0,1,0,1,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,1,0,1,0,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1],
        [1,1,1,1,1,14,1,1,1,1,1,1,1,1,1,1,1,1],
      ],
      entry: { x: 1, y: 3 },
      exit: { x: 10 , y: 20}
    },
  ],
  medium: [  
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,1,0,0,0,0,0,1,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,1,0,1,0,0,1],
        [1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,0,1,1,1,1],
        [1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,0,1,0,0,0,0,1],
        [1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,1,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1],
        [1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1],
        [1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,14,1],
      ],
      entry: { x: 1, y: 9 },
      exit: { x: 23, y: 20 }
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,1,0,0,1,0,0,1,0,1,0,0,0,1,0,0,0,1],
        [1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1],
        [1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,0,1,0,1,0,0,1,0,1,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,1],
        [1,1,1,0,1,0,1,0,1,1,1,1,1,1,0,1,0,1,0,1,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,14,1],
      ],
      entry: { x: 1, y: 16 },
      exit: { x: 23, y: 20 }
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,1],
        [1,0,1,1,1,1,1,1,0,1,0,0,0,1,0,1,1,1,0,1,1,1,1],
        [1,0,0,0,0,0,0,1,0,1,1,1,1,1,0,0,0,1,0,1,0,0,1],
        [1,1,1,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,0,1],
        [1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1],
        [1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,0,0,0,0,1,1,0,1,1,1,1,1,1],
        [1,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1],
        [1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1],
        [1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,1,1,0,0,0,1,0,1],
        [1,0,0,0,1,0,0,0,1,1,1,0,1,0,0,0,0,0,1,1,1,0,1],
        [1,1,1,0,1,0,1,1,1,0,0,0,1,1,1,0,1,0,0,0,1,0,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,0,1,1,0,1,1,1,0,1,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,1,0,0,1,0,1,0,0,0,1,0,1],
        [1,0,0,0,1,0,0,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,14,1],
      ],
      entry: { x: 21, y: 3 },
      exit: { x: 23, y: 20 }
    },
    {
      maze: [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,1],
        [1,1,1,0,1,0,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,1],
        [1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1],
        [1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,0,1],
        [1,0,0,0,1,0,1,0,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1],
        [1,0,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1],
        [1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1],
        [1,1,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,1],
        [1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1],
        [1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,14,1,1,1,1,1,1,1,1,1,1,1],
      ],
      entry: { x: 1, y: 3 },
      exit: { x: 13, y: 20 }
    },
    
  ],
  hard: [ 
    {
      maze: [
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
      [1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1],
      [1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0,1,1,1,1,1],
      [1,0,1,0,1,0,1,0,1,1,1,1,0,1,1,1,1,0,1,0,1,0,0,0,0,0,1],
      [1,0,1,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,1,1,1,1,1,0,1],
      [1,0,1,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,0,1,0,0,0,0,0,1],
      [1,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,1,0,1,1,1,1,1],
      [1,1,1,1,1,0,1,0,1,1,1,1,0,1,1,1,1,1,1,0,1,0,0,0,0,0,1],
      [1,0,0,0,0,0,1,0,0,0,0,0,4,0,0,0,0,0,1,0,1,1,1,1,1,1,1],
      [1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,0,0,0,0,0,0,0,1],
      [1,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,1,1,1,1,1,1,0,1],
      [1,1,1,1,1,0,1,1,1,1,0,1,0,1,0,1,1,1,1,0,0,0,0,0,1,0,1],
      [1,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,1,1,1,0,1,0,1],
      [1,0,1,1,1,0,1,0,1,1,1,1,0,1,1,1,1,0,1,0,1,0,1,0,1,0,1],
      [1,0,0,0,0,0,1,0,0,0,0,1,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1],
      [1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,0,1,1,1,0,1,0,1,0,1,0,1],
      [1,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1],
      [1,14,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    ],
      entry: { x: 12 , y: 10},
      exit: { x: 1, y: 19 }
  },
  {
    maze: [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0],
    [0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0],
    [0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0],
    [0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0],
    [1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1],
    [1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1],
    [1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1],
    [1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,1],
    [1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1],
    [1,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,1],
    [1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1],
    [1,0,0,1,1,0,0,1,0,0,0,1,1,1,0,0,0,1,0,0,0,0,0,0,1,0,1],
    [1,0,0,0,1,0,0,0,0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,1],
    [1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1],
    [0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0],
    [0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0],
    [0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0],
    [0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,1,1,1,1,1,0,0,0,0],
  ],
    entry: { x: 1 , y: 7},
    exit: { x: 20, y: 18 }
  },
  ]
};

let currentDifficulty = 'easy'; 

document.getElementById('difficulty-select').addEventListener('change', function() {
  currentDifficulty = this.value;
  currentLevelIndex = 0; 
  loadNewLevel(game.scene.scenes[0]); 
});

function preload() {
  console.log('preload')
  this.load.image('player', 'http://localhost:8004/assets/djs.PNG');
  this.load.image('block', 'http://localhost:8004/assets/orange.PNG');
}
function create() {
  console.log('create')
  loadLevel(this);
  cursors = this.input.keyboard.createCursorKeys();
  this.commandText = this.add.text(config.width / 2, 30, 'Listening...', {
    font: '20px Arial',
    fill: '#000000',
    fontWeight: 'bold'
  }).setOrigin(0.5);

  this.commandResultText = this.add.text(config.width / 2, this.commandText.y + 30, '', {
    font: '20px Arial',
    fill: '#000000',
    fontWeight: 'bold'
  }).setOrigin(0.5);

  console.log('Command Text Initialized:', this.commandText);
  console.log('Command Result Text Initialized:', this.commandResultText);

  const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
  const recognition = new SpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = false;
  recognition.lang = 'en-US';

  recognition.onresult = (event) => {
    const transcript = event.results[event.results.length - 1][0].transcript.trim().toLowerCase();
    command = transcript
    console.log('Raw transcript:', transcript);

    if (transcript.includes('right') || transcript.includes('left') || transcript.includes('down') || transcript.includes('up') || transcript.includes('write') || transcript.includes('done')) {
      this.commandText.setText('Command Detected');
  } else {
      this.commandText.setText('Say Again!');
  }
    console.log('Recognized command:', transcript);
    handleVoiceCommand();
  };

  recognition.onerror = (event) => {
    console.error('Speech recognition error:', event.error);
  };

  recognition.start();

  document.getElementById('up-btn').addEventListener('click', () => movePlayer('up'));
  document.getElementById('down-btn').addEventListener('click', () => movePlayer('down'));
  document.getElementById('left-btn').addEventListener('click', () => movePlayer('left'));
  document.getElementById('right-btn').addEventListener('click', () => movePlayer('right'));
}

function movePlayer(direction){
  if (direction == 'up'){
    player.setVelocityY(-300);

  }if (direction == 'down'){
    player.setVelocityY(300);

  }if (direction == 'left'){
    player.setVelocityX(-300);

  }if (direction == 'right'){
    player.setVelocityX(300);

  }
}

function updateArrow() {
  if (cursors.left.isDown) {
    player.setVelocityX(-300);
  } else if (cursors.right.isDown) {
    player.setVelocityX(300);
  }

  if (cursors.up.isDown) {
    player.setVelocityY(-300);
  } else if (cursors.down.isDown) {
    player.setVelocityY(300);
  }

  if (checkIfReachedExit(levels[currentDifficulty][currentLevelIndex].exit) && !popupShown) {
    popupShown = true;
    showPopup(this);
  }

  
  
}


function loadLevel(scene) {
  const level = levels[currentDifficulty][currentLevelIndex];
  const offsetX = (config.width - level.maze[0].length * mazeSize) / 2;
  const offsetY = (config.height - level.maze.length * mazeSize) / 2;

  createMaze(scene, level.maze, offsetX, offsetY);

  const entryPoint = findEntryPoint(level.entry, offsetX, offsetY);
  player = scene.physics.add.sprite(entryPoint.x, entryPoint.y, 'player');
  player.setScale(0.7);
  player.setCollideWorldBounds(true);

  cursors = scene.input.keyboard.createCursorKeys();

  scene.physics.add.collider(player, walls);
}

function createMaze(scene, maze, offsetX, offsetY) {
  walls = scene.physics.add.staticGroup();

  for (let y = 0; y < maze.length; y++) {
      for (let x = 0; x < maze[y].length; x++) {
          if (maze[y][x] === 1) {
              const block = walls.create(mazeSize * (x + 0.5) + offsetX, mazeSize * (y + 0.5) + offsetY, 'block');
              block.setScale(0.2);
              block.refreshBody();
          }
      }
  }
}

function findEntryPoint(entry, offsetX, offsetY) {
  return { x: mazeSize * (entry.x + 0.5) + offsetX, y: mazeSize * (entry.y + 0.5) + offsetY };
}

let commandQueue = [];
let isMoving = false;
let targetVelocityX = 0;
let targetVelocityY = 0;

function handleVoiceCommand() {
  if (!player) return;

  commandQueue = command.split(' ').filter(word => word.trim());
  console.log(commandQueue);

  if (!isMoving && commandQueue.length > 0) {
    processNextCommand();
  }
}

function processNextCommand() {
  if (commandQueue.length === 0) {
    isMoving = false;
    return;
  }

  let command = commandQueue.shift(); 

  if (command.includes('right') || command.includes('write')) {
    targetVelocityX = 300;
    targetVelocityY = 0;
  } else if (command.includes('left')) {
    targetVelocityX = -300;
    targetVelocityY = 0;
  } else if (command.includes('up')) {
    targetVelocityX = 0;
    targetVelocityY = -300;
  } else if (command.includes('down') || command.includes('done')) {
    targetVelocityX = 0;
    targetVelocityY = 300;
  }

  player.setVelocity(targetVelocityX, targetVelocityY);
  isMoving = true;
}

function update() {
  updateArrow(); 

  if (isMoving && player.body.velocity.x === 0 && player.body.velocity.y === 0) {
    isMoving = false;
    processNextCommand();
  }

  if (checkIfReachedExit(levels[currentDifficulty][currentLevelIndex].exit) && !popupShown) {
    popupShown = true;
    showPopup(this);
  }
}


function checkIfReachedExit(exit) {
  const exitX = mazeSize * (exit.x + 0.5);
  const exitY = mazeSize * (exit.y + 0.5);
  const distance = Phaser.Math.Distance.Between(player.x, player.y, exitX, exitY);
  return distance < mazeSize / 2;
}

function nextLevel(scene) {
  currentLevelIndex++;
  if (currentLevelIndex >= levels[currentDifficulty].length) {
      currentLevelIndex = 0;
  }
  loadNewLevel(scene);
  popupShown = false;
}

function loadNewLevel(scene) {
  if (player) player.destroy();
  if (walls) walls.clear(true, true); 

  loadLevel(scene);
}

function showPopup(scene) {
  console.log("Popup called");
  
  const popup = document.getElementById('popup');
  popup.style.display = 'block';
  
  const nextMazeButton = document.getElementById('nextMazeButton');
  const restartMazeButton = document.getElementById('restartMazeButton');
  
  nextMazeButton.removeEventListener('click', handleNextMazeClick);
  restartMazeButton.removeEventListener('click', handleRestartMazeClick);
  
  nextMazeButton.addEventListener('click', handleNextMazeClick);
  restartMazeButton.addEventListener('click', handleRestartMazeClick);
}
  
function handleNextMazeClick() {
  const popup = document.getElementById('popup');
  popup.style.display = 'none';
  
  if (!next_level) {
    next_level = true;
    nextLevel(game.scene.scenes[0]);
  }
  
  popupShown = false;
  next_level = false;
}
  
function handleRestartMazeClick() {
  const popup = document.getElementById('popup');
  popup.style.display = 'none';
  
  game.scene.scenes[0].scene.restart();  
  popupShown = false;
}
  
function nextLevel(scene) {
  console.log("Next level called");
  currentLevelIndex++;
  if (currentLevelIndex >= levels.length) {
    currentLevelIndex = 0; 
  }
  scene.scene.restart(); 
  popupShown = false; 
}
